# LICENSE

###### 2017 - 2018

The source code on this project can be found on [Gitlab](https://gitlab.com/yunocare/EMS-FeatureHouse).

The project "EMS" is based on an open source project named *"Employee Payroll Management System"* 
published by user *"Hyrex"* (refined the project's state from: 2016-09-30). Source code on this
project can be found [here](https://www.dropbox.com/s/rmiuenk9oeqo3pw/Employee%20Payroll%20System.zip?dl=0),
as well as on Hyrex's [channel](https://www.youtube.com/channel/UC12Z6-QyYjcGmxgaLIxmFwg).

The project itself is advanced to an Employee Management System with additional functionalities for Administration and Report Generation. 
This project is created for **academic purpose** in the context software product line development using FeatureIDE plugin in Eclipse IDE.
See FeatureIDE's [official Documentation](https://featureide.github.io/) for further information.