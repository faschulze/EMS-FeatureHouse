package de.ovgu.isp.ems.core;

public class AccessControl {

	/**
	 * empty constructor
	 */
	public AccessControl() {

	}
	
	/**
	 * ADD NEW USER ROLES HERE:
	 * 
	 * 
	 * @return a list of valid user roles
	 */
	public String[] getAllUserRoles() {
		String[] userroles = new String[3];
		userroles[0] = "Admin";
		userroles[1] = "Sales";
		userroles[2] = "HR";
		return userroles;

	}

}