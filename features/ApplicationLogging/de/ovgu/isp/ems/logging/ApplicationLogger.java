package de.ovgu.isp.ems.logging;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The ApplicationLogger's configuration can be found inside the log4j2.xml
 * file. Since the ApplicationLogging feature is spread through the overall
 * application, the logging is implemented using the Antenna preprocessor
 * composer.
 * 
 * @author fs
 *
 */
public class ApplicationLogger {

	// by default the logging level is TRACE:
	private Logger logger = LogManager.getRootLogger();

	/**
	 * constructors checks if log directory exists
	 */
	public ApplicationLogger() {
		makeDirectory();
	}

	/**
	 * 
	 * @return the application logger
	 */
	public Logger getApplicationLogger() {
		return logger;
	}

	/**
	 * method will create a logs/ directory inside the project's folder, if it does
	 * not exist yet.
	 */
	private void makeDirectory() {
		File logDirectory = new File("." + File.separator + "logs");
		// if the directory does not exist, create it
		if (!logDirectory.exists()) {
			System.out.println("creating directory: " + logDirectory.getName());
			boolean result = false;

			try {
				logDirectory.mkdir();
				result = true;
			} catch (SecurityException se) {
				// handle it
				se.printStackTrace();
			}
			if (result) {
				System.out.println("DIR created");
				logger.info("logs/ directory successfully created.");
			}
		}
	}

}
