package de.ovgu.isp.ems.ui.core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import de.ovgu.isp.ems.ui.ReportGenerator;
import de.ovgu.isp.ems.ui.UIFeature;

public class MainMenu {
	
	//<Feature:ReportGenerator>
	
	JButton jButton8 = new JButton();

	UIFeature reportGenerator = new UIFeature(messages.getString("report_generator"), "/Images/slip.png",
			new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					buttonActionReportGenerator(evt);
				}
			}, jButton8); 		// TODO: find new image icon
	/**
	 * this method add the feature report generator to the arraylist in the main	 * menu
	 */
	private void addUIFeatureToList() {
		//TODO: test BUGFIX: why is this not composing as expected??!?
		original();
	
		featureList.add(reportGenerator);
	}
	
	/**
	 * hook method in feature ListMenuUI
	 * 
	 * @param clickedItem clicked list item on jlist
	 */
	private void compareWithClickedItemAction(String clickedItem) {
		original(clickedItem);
		if (reportGenerator.getFeatureName().equals(clickedItem)) {
			buttonActionReportGenerator(null);
		}
	}

	/**
	 * Button: Report Generator
	 * 
	 * @param evt
	 */
	private void buttonActionReportGenerator(ActionEvent evt) {
		System.out.println("Report Generator clicked");
		ReportGenerator _rg = new ReportGenerator();
		_rg.setVisible(true);
	}
	
	//workaround method - supposed to be replaced:
	private void initializeRPGButtonUI() {
		original();

		featureList.add(reportGenerator);
		
		// Initialize REPORTS Menu (second left in menu bar)
		jMenu5.setText(messages.getString("reports"));
		// Add Employee Reports to Menu
		jMenuItem6.setText(messages.getString("emp_reports"));
		jMenuItem6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				itemActionGenerateEmpReport(evt);
			}
		});
		jMenu5.add(jMenuItem6);

		jMenuItem7.setText(messages.getString("emp_total_allowanceRP"));
		jMenuItem7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				itemActionGenerateAllowanceReport(evt);
			}
		});
		jMenu5.add(jMenuItem7);

		jMenuItem2.setText(messages.getString("emp_total_deduction"));
		jMenuItem2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				itemActionGenerateEmpDeductionsReport(evt);
			}
		});
		jMenu5.add(jMenuItem2);

		jMenuBar1.add(jMenu5);
	}
	

	//</Feature:ReportGenerator>

}