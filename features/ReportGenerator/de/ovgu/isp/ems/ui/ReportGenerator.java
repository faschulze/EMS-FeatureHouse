package de.ovgu.isp.ems.ui;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;


/**
*
* @author fs
*/
public class ReportGenerator extends JFrame {

   /**
    * Creates new form ReportGenerator
    */
   public ReportGenerator() {
       initComponents();
   }

   @SuppressWarnings("unchecked")
   private void initComponents() {
	   
	   //TODO: put messages.getString("rg") to here:
	   setTitle("EMS - " + "Report Generator");
       setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);


       jTabbedPane1 = new JTabbedPane();
       jPanel1 = new JPanel();
       jComboBox1 = new JComboBox<String>();
       jButton1 = new JButton();
       jLabel1 = new JLabel();
       jPanel2 = new JPanel();
       jComboBox2 = new JComboBox<String>();
       jButton2 = new JButton();
       jLabel2 = new JLabel();
       jPanel3 = new JPanel();
       jComboBox3 = new JComboBox<String>();
       jButton3 = new JButton();
       jLabel3 = new JLabel();


       jComboBox1.setModel(new DefaultComboBoxModel<String>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
       jComboBox1.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               jComboBox1ActionPerformed(evt);
           }
       });

       jButton1.setText("Generate Report");

       jLabel1.setText("You can generate reports......and some text..........");

       GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
       jPanel1.setLayout(jPanel1Layout);
       jPanel1Layout.setHorizontalGroup(
           jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
           .addGroup(jPanel1Layout.createSequentialGroup()
               .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                   .addGroup(jPanel1Layout.createSequentialGroup()
                       .addContainerGap()
                       .addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 329, GroupLayout.PREFERRED_SIZE))
                   .addGroup(jPanel1Layout.createSequentialGroup()
                       .addGap(282, 282, 282)
                       .addComponent(jButton1))
                   .addGroup(jPanel1Layout.createSequentialGroup()
                       .addGap(246, 246, 246)
                       .addComponent(jComboBox1, GroupLayout.PREFERRED_SIZE, 221, GroupLayout.PREFERRED_SIZE)))
               .addContainerGap(316, Short.MAX_VALUE))
       );
       jPanel1Layout.setVerticalGroup(
           jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
           .addGroup(jPanel1Layout.createSequentialGroup()
               .addContainerGap()
               .addComponent(jLabel1)
               .addGap(101, 101, 101)
               .addComponent(jComboBox1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
               .addGap(52, 52, 52)
               .addComponent(jButton1)
               .addContainerGap(208, Short.MAX_VALUE))
       );

       jTabbedPane1.addTab("XML Generator", jPanel1);

       jComboBox2.setModel(new DefaultComboBoxModel<String>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
       jComboBox2.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               jComboBox2ActionPerformed(evt);
           }
       });

       jButton2.setText("Generate Report");

       jLabel2.setText("You can generate reports......and some text..........");

       GroupLayout jPanel2Layout = new GroupLayout(jPanel2);
       jPanel2.setLayout(jPanel2Layout);
       jPanel2Layout.setHorizontalGroup(
           jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
           .addGroup(jPanel2Layout.createSequentialGroup()
               .addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                   .addGroup(jPanel2Layout.createSequentialGroup()
                       .addContainerGap()
                       .addComponent(jLabel2, GroupLayout.PREFERRED_SIZE, 329, GroupLayout.PREFERRED_SIZE))
                   .addGroup(jPanel2Layout.createSequentialGroup()
                       .addGap(282, 282, 282)
                       .addComponent(jButton2))
                   .addGroup(jPanel2Layout.createSequentialGroup()
                       .addGap(246, 246, 246)
                       .addComponent(jComboBox2, GroupLayout.PREFERRED_SIZE, 221, GroupLayout.PREFERRED_SIZE)))
               .addContainerGap(316, Short.MAX_VALUE))
       );
       jPanel2Layout.setVerticalGroup(
           jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
           .addGroup(jPanel2Layout.createSequentialGroup()
               .addContainerGap()
               .addComponent(jLabel2)
               .addGap(101, 101, 101)
               .addComponent(jComboBox2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
               .addGap(52, 52, 52)
               .addComponent(jButton2)
               .addContainerGap(208, Short.MAX_VALUE))
       );

       jTabbedPane1.addTab("PDF Generator", jPanel2);

       jComboBox3.setModel(new DefaultComboBoxModel<String>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
       jComboBox3.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               jComboBox3ActionPerformed(evt);
           }
       });

       jButton3.setText("Generate Report");

       jLabel3.setText("You can generate reports......and some text..........");

       GroupLayout jPanel3Layout = new GroupLayout(jPanel3);
       jPanel3.setLayout(jPanel3Layout);
       jPanel3Layout.setHorizontalGroup(
           jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
           .addGroup(jPanel3Layout.createSequentialGroup()
               .addGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                   .addGroup(jPanel3Layout.createSequentialGroup()
                       .addContainerGap()
                       .addComponent(jLabel3, GroupLayout.PREFERRED_SIZE, 329, GroupLayout.PREFERRED_SIZE))
                   .addGroup(jPanel3Layout.createSequentialGroup()
                       .addGap(282, 282, 282)
                       .addComponent(jButton3))
                   .addGroup(jPanel3Layout.createSequentialGroup()
                       .addGap(246, 246, 246)
                       .addComponent(jComboBox3, GroupLayout.PREFERRED_SIZE, 221, GroupLayout.PREFERRED_SIZE)))
               .addContainerGap(316, Short.MAX_VALUE))
       );
       jPanel3Layout.setVerticalGroup(
           jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
           .addGroup(jPanel3Layout.createSequentialGroup()
               .addContainerGap()
               .addComponent(jLabel3)
               .addGap(101, 101, 101)
               .addComponent(jComboBox3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
               .addGap(52, 52, 52)
               .addComponent(jButton3)
               .addContainerGap(208, Short.MAX_VALUE))
       );

       jTabbedPane1.addTab("CSV Generator", jPanel3);

       GroupLayout layout = new GroupLayout(getContentPane());
       getContentPane().setLayout(layout);
       layout.setHorizontalGroup(
           layout.createParallelGroup(GroupLayout.Alignment.LEADING)
           .addComponent(jTabbedPane1)
       );
       layout.setVerticalGroup(
           layout.createParallelGroup(GroupLayout.Alignment.LEADING)
           .addComponent(jTabbedPane1)
       );

       pack();
   }// </editor-fold>                        

   private void jComboBox1ActionPerformed(ActionEvent evt) {                                           
       // TODO add your handling code here:
   }                                          

   private void jComboBox2ActionPerformed(ActionEvent evt) {                                           
       // TODO add your handling code here:
   }                                          

   private void jComboBox3ActionPerformed(ActionEvent evt) {                                           
       // TODO add your handling code here:
   }                                          

   /**
    * @param args the command line arguments
    */
   public static void main(String args[]) {
     

       java.awt.EventQueue.invokeLater(new Runnable() {
           public void run() {
               new ReportGenerator().setVisible(true);
           }
       });
   }

   // Variables declaration - do not modify                     
   private JButton jButton1;
   private JButton jButton2;
   private JButton jButton3;
   private JComboBox<String> jComboBox1;
   private JComboBox<String> jComboBox2;
   private JComboBox<String> jComboBox3;
   private JLabel jLabel1;
   private JLabel jLabel2;
   private JLabel jLabel3;
   private JPanel jPanel1;
   private JPanel jPanel2;
   private JPanel jPanel3;
   private JTabbedPane jTabbedPane1;
   // End of variables declaration     
}