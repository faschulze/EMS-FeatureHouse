package de.ovgu.isp.ems.core;

/**
 * 
 * @author fs
 *
 */
public class Theme {

	// <Feature: Theme2>
	String _imageLocation = "/Images/bk2.jpg";

	/**
	 * 
	 * @return the theme's background image location
	 */
	public String getThemeBackground() {
		return _imageLocation;
	}
	// </Feature:Theme2>

}