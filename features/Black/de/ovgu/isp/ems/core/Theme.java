package de.ovgu.isp.ems.core;
import java.awt.Color;

public class Theme {
	
	//<Feature:Black>
	
	Color _color = Color.BLACK;
	
	/**
	 * 
	 * @return theme color
	 */
	public Color getColor() {
		return _color;
	}
	
	//</Feature:Black>
	
}