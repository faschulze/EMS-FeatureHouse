package de.ovgu.isp.ems.ui.core;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import de.ovgu.isp.ems.core.Localization;
import de.ovgu.isp.ems.core.Theme;
import de.ovgu.isp.ems.ui.UIFeature;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.itextpdf.awt.geom.misc.Messages;
import com.sun.corba.se.impl.ior.GenericTaggedComponent;

import java.awt.List;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import de.ovgu.isp.ems.ui.UIFeature;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;

public class MainMenu {

	ArrayList<UIFeature> featureList;

	JPanel listpanel = new JPanel();
	JTextArea output;
	JList list;
	JTable table;
	ListSelectionModel listSelectionModel;
	JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

	private void putAllSelectedFeaturesToPanel() {
		original();
		// put all features from featureList to JPanel:
		listpanel.setLayout(new BorderLayout());

		// initialize String list:
		String[] listData = new String[featureList.size()];
		for (int i = 0; i < listData.length; i++) {
			listData[i] = featureList.get(i).getFeatureName();
		}

		// declare new list
		list = new JList(listData);
		listSelectionModel = list.getSelectionModel();
		listSelectionModel.addListSelectionListener(new SharedListSelectionHandler());

		/*
		 * MouseListener to find clicked list entry
		 */
		MouseListener mouseListener = new MouseAdapter() {
			public void mouseClicked(MouseEvent mouseEvent) {
				JList theList = (JList) mouseEvent.getSource();
				if (mouseEvent.getClickCount() == 1) {
					int index = theList.locationToIndex(mouseEvent.getPoint());
					if (index >= 0) {
						Object o = theList.getModel().getElementAt(index);
						String clickedItem = o.toString();
						System.out.println("Clicked on: " + o.toString());
						compareWithClickedItemAction(clickedItem);
						for (int i = 0; i < featureList.size(); i++) {

							if (clickedItem.equals(featureList.get(i).getFeatureName())) {
								System.out.println("## - " + featureList.get(i).getFeatureName());
							}

						}
					}
				}
			}
		};
		list.addMouseListener(mouseListener);
		// initialize split pane:
		initializeSplitPane();
		jPanel2.add(splitPane);
		splitPane.setBounds(_ybound/2, _xbound, _xlength, _ylength);
		// bounds:
		System.out.println("# ListMenuUI: " + _xlength + "," + _ylength + "," + _xbound + "," + _ybound);
		
	}

	private void compareWithClickedItemAction(String clickedItem) {

		if (clickedItem.equals(messages.getString("deduction"))) {
			buttonActionUsers(null);
		} else if (clickedItem.equals(messages.getString("allowance"))) {
			buttonActionAllowance();
		} else if (clickedItem.equals(messages.getString("payment"))) {
			buttonActionSearchSalarySlip();
		}

	}

	private void initializeSplitPane() {
		JScrollPane listPane = new JScrollPane(list);

		// Build output area.
		output = new JTextArea(1, 10);
		output.setEditable(false);
		//TODO: add translation to .properties
		output.append("Welcome to EMS portal.");
		JScrollPane outputPane = new JScrollPane(output, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		// Do the layout.
		listpanel.add(splitPane, BorderLayout.CENTER);

		JPanel topHalf = new JPanel();
		topHalf.setLayout(new BoxLayout(topHalf, BoxLayout.LINE_AXIS));
		JPanel listContainer = new JPanel(new GridLayout(1, 1));
		// TODO: add "overview" to .properties
		listContainer.setBorder(BorderFactory.createTitledBorder("Overview"));
		listContainer.add(listPane);

		topHalf.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
		topHalf.add(listContainer);
		// topHalf.add(tableContainer);

		// set tophalf size:
		topHalf.setMinimumSize(new Dimension(100, 300));
		topHalf.setPreferredSize(new Dimension(100, 110));
		splitPane.add(topHalf);

		JPanel bottomHalf = new JPanel(new BorderLayout());
		bottomHalf.add(outputPane, BorderLayout.CENTER);

		// set buttomHalf size:
		bottomHalf.setMinimumSize(new Dimension(400, 50));
		bottomHalf.setPreferredSize(new Dimension(450, 135));
		splitPane.add(bottomHalf);

	}

	/**
	 * Source Code from
	 * https://docs.oracle.com/javase/tutorial/uiswing/events/listselectionlistener.html
	 * 
	 */
	class SharedListSelectionHandler implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {
			ListSelectionModel lsm = (ListSelectionModel) e.getSource();

			int firstIndex = e.getFirstIndex();
			int lastIndex = e.getLastIndex();
			boolean isAdjusting = e.getValueIsAdjusting();

			if (lsm.isSelectionEmpty()) {
			} else {
				// Find out which indexes are selected.
				int minIndex = lsm.getMinSelectionIndex();
				int maxIndex = lsm.getMaxSelectionIndex();
				for (int i = minIndex; i <= maxIndex; i++) {
					if (lsm.isSelectedIndex(i)) {
						String date = new Localization().getCountrySpecificDate();
						String time = new Localization().getTimeString();
						output.setText(date + " - " + time +"\n" + featureList.get(i).getFeatureName());
						System.out.println(" " + i);
						// output.append(featureList.get(i).getFeatureName() + " --> " + "Event for
						// indexes " + firstIndex
						// + " - " + lastIndex + "; isAdjusting is " + isAdjusting + "; selected
						// indexes:");
					}
				}
				output.append("\n");
				output.setCaretPosition(output.getDocument().getLength());
			}
		}
	}
}
