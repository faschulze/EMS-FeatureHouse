package de.ovgu.isp.ems.core;
import java.awt.Color;
import java.awt.Font;

public class Theme {

	//TODO: !! set font to Arial
	int _font = Font.ITALIC;
	//TODO: declare as private
	String _name = "Arial";

	public int getFont() {
		return _font;
	}

	public String getName() {
		return _name;
	}
	
	public Font getThemeFont() {
		return new Font("Tahoma", 1, getSize());
		
	}
}
