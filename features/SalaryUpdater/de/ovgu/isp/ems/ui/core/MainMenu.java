package de.ovgu.isp.ems.ui.core;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import de.ovgu.isp.ems.ui.UIFeature;
import de.ovgu.isp.ems.ui.updateSalary;

public class MainMenu {

	UIFeature salaryUpdater = new UIFeature(messages.getString("update_salary"), "/Images/Update.png",
			new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					openUpdateSalaryDialog(evt);
				}
			}, jButton5);

	/**
	 * this method add the feature salaryUpdater to the arraylist in the main menu
	 */
	private void addUIFeatureToList() {
		original();
		featureList.add(salaryUpdater);
	}

	/**
	 * hook method in feature ListMenuUI
	 * 
	 * @param clickedItem
	 *            clicked list item on jlist
	 */
	private void compareWithClickedItemAction(String clickedItem) {
		original(clickedItem);
		if (salaryUpdater.getFeatureName().equals(clickedItem)) {
			openUpdateSalaryDialog(null);
		}
	}

	/**
	 * method opens an update salary JFrame
	 * 
	 * @param evt
	 */
	private void openUpdateSalaryDialog(ActionEvent evt) {

		if (updateSalary.updateSalaryLock) {
			// #ifdef ApplicationLogging
			// @log.error("failed to open updateSalaryDialog, since updateSalaryLock is " +
			// @ "currently locked");
			// #endif

		} else {
			updateSalary x = new updateSalary();
			x.setVisible(true);
			// #ifdef ApplicationLogging
			// @ log.info("update salary report opened by user " + Emp.empId);
			// @log.info("setting updateSalaryLock to locked");
			// #endif
			updateSalary.updateSalaryLock = true;
		}
	}

}
