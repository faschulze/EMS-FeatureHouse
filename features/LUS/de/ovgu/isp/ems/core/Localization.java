package de.ovgu.isp.ems.core;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * 
 * @author fs
 *
 */
public class Localization {

	// <Feature:LUS>
	/**
	 * 
	 * @return US date format
	 */
	public String getCountrySpecificDate() {
		Calendar cal = new GregorianCalendar();
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		return " | " + year + "-" + (month + 1) + "-" + day + " | ";
	}

	/**
	 * 
	 * @return united states
	 */
	public Locale getCurrentLocale() {
		Locale _locale = new Locale("en", "US");
		return _locale;
	}
	// </Feature:LUS>

}
