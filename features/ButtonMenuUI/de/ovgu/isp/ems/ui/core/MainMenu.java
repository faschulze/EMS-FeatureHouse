package de.ovgu.isp.ems.ui.core;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import de.ovgu.isp.ems.ui.UIFeature;

/**
 * 
 * @author fs
 *
 */
public class MainMenu {
	
	/*
	 * Main Menu JFrame configuration in ButtonMenuUI:
	 */
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	int window_height = (int)(screenSize.getHeight());
	int window_width = (int)(screenSize.getWidth())/2;
	
	JButton jButton1 = new JButton();
	JButton jButton9 = new JButton();
	
	
	// <Feature:ButtonMenuUI>
	ArrayList<UIFeature> featureList;

	/**
	 * 
	 */
	public void putAllSelectedFeaturesToPanel() {
		original();

		System.out.println("refining method....");
		// put all features from featureList to JPanel:
		for (int i = 0; i < featureList.size(); i++) {

			// #ifdef ApplicationLogging
			// @ log.info("add button "+ featureList.get(i).getFeatureName() +
			// @ " to jpanel.");
			// #endif

			JButton btn = featureList.get(i).getFeatureButton();
			System.out.println("## " + featureList.get(i).getFeatureName()); // TODO: replace
			btn.setText(featureList.get(i).getFeatureName());
			btn.setFont(_font);
			btn.setForeground(_fontColor);
			btn.setIcon(new ImageIcon(getClass().getResource(featureList.get(i).getFeatureImagePath())));
			btn.addActionListener(featureList.get(i).getFeatureListener());
			jPanel2.add(btn);

			// TODO: set location for buttons --> Array Dimension --> currently:
			// setAllSelectedFeaturesToLocation()

			// #ifdef ApplicationLogging
			// @ log.info("button "+ featureList.get(i).getFeatureName() +
			// @ " successfully added.");
			// #endif

		}

	}

	/**
	 * 
	 */
	private void setAllSelectedFeaturesToLocation() { // only if feature ButtonMenuUI is selected
		original();
		// #ifdef ApplicationLogging
		// @ log.info("set all selected feature buttons to specific position" +
		// @ "on jpanel");
		// #endif

		// TODO: !! find algorithm to set buttons to specified location
		jButton1.setBounds(_buttonY, _buttonX + _buttonhight, _buttonwidth, _buttonhight);
		jButton3.setBounds(_buttonY, _buttonX + (_buttonhight * 3), _buttonwidth, _buttonhight);
		jButton2.setBounds(_buttonY, _buttonX + (_buttonhight * 2), _buttonwidth, _buttonhight);
		jButton4.setBounds(_buttonY + _buttonwidth + _buttonYoffset, _buttonX + _buttonhight, _buttonwidth,
				_buttonhight);

		jButton7.setBounds(_buttonY + _buttonwidth + _buttonYoffset, _buttonX + (_buttonhight * 3), _buttonwidth,
				_buttonhight);
		jButton5.setBounds(_buttonY + _buttonwidth + _buttonYoffset, _buttonX, _buttonwidth, _buttonhight);
		// set bound for jButton8
		jButton8.setBounds(_buttonY + _buttonwidth + _buttonYoffset, _buttonX + (_buttonhight * 2), _buttonwidth,
				_buttonhight);
		jButton9.setBounds(_buttonY, _buttonX, _buttonwidth, _buttonhight);
		// ................

	}
	
	//hook method:
		private void compareWithClickedItemAction(String clickedItem) {
			//TODO: remove
		}

	// </Feature:ButtonMenuUI>

}
