package de.ovgu.isp.ems.core;

import java.awt.Color;

public class Theme {
	
	//<Feature:Blue>
	
	Color _color = Color.BLUE;
	
	/**
	 * 
	 * @return theme color
	 */
	public Color getColor() {
		return _color;
	}
	
	//</Feature:Blue>
}