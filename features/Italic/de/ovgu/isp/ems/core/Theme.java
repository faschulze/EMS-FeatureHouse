package de.ovgu.isp.ems.core;

import java.awt.Font;

public class Theme {

	int _font = Font.ITALIC;
	String _name = "Italic";

	public int getFont() {
		return _font;
	}

	public String getName() {
		return _name;
	}

	public Font getThemeFont() {
		return new Font("Tahoma", 1, getSize());
		
	}

}
