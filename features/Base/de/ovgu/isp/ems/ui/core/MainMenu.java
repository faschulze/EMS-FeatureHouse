package de.ovgu.isp.ems.ui.core;

import com.itextpdf.text.pdf.PdfWriter;
import javax.swing.GroupLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import com.itextpdf.text.Document;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import java.awt.Font;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.logging.log4j.Logger;
//import all optional components
import de.ovgu.isp.ems.*;
import de.ovgu.isp.ems.ui.*;
import de.ovgu.isp.ems.core.Theme;
import de.ovgu.isp.ems.db.db;
import de.ovgu.isp.ems.db.Emp;
import de.ovgu.isp.ems.core.Localization;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;

/**
 * 
 * @author fs
 *
 */
public class MainMenu extends JFrame {
	private static final long serialVersionUID = 1L;
	
	Connection conn = null;
	ResultSet rs = null;
	PreparedStatement pst = null;
	// int panel_height;
	// int panel width;

	ArrayList<UIFeature> featureList = new ArrayList<UIFeature>();

	/**
	 * Creates new JFrame MainMenu
	 */
	public MainMenu() {
		initComponents();
		conn = db.java_db();

	}

	// HOOK METHODS
	private void buttonActionEmployeeDeduction() { //only if feature Allowance is selected
		
	}
	private void buttonActionAllowance() {//only if feature Allowance is selected
		
	}
	private void buttonActionSearchSalarySlip() {//only if feature Allowance is selected
		
	}
	private void initializeAllowanceButtonUI(){//only if feature Allowance is selected
		
	}
	private void initalizeAuditButtonUI() {// only if feature AuditTrail is selected
	}

	private void writeLogoutToAuditDetails() {// only if feature AuditTrail is selected
	}

	private void initalizeEmpAdderButtonUI() {// only if feature EmployeeAdder is selected
	}

	private void initializeSalaryUpdaterButtonUI() {// only if feature SalaryUpdater is selected
	}

	private void initializeRPGButtonUI() { // only if feature ReportGenerator is selected

	}

	private void printEmployeeDeductionReportAsPDF(String filePath) {
		// only if feature PDF Report is selected
	}

	private void printAllowanceReportAsPDF(String filePath) {
		// only if feature PDF Report is selected
	}

	private void printEmployeeReportAsPDF(String filePath) {
		// only if feature PDF Report is selected
	}

	private void addUIFeatureToList() {
		// TODO: !! method should be implemented by all UI features
	}

	public void putAllSelectedFeaturesToPanel() { // if feature ButtonMenuUI or ListMenuUI is selected

	}

	private void setAllSelectedFeaturesToLocation() { // only if feature ButtonMenuUI is selected

	}

	// END HOOK METHODS

	/**
	 * 
	 */
	private void initComponents() { // GEN-BEGIN:initComponents
		
		setTitle("Employee Management System - "+ messages.getString("main_menu"));

		setSize(window_height, window_width);
		// Panel:
		jPanel2 = new JPanel();

		// Menu:
		jMenu1 = new JMenu();
		jMenu3 = new JMenu();
		jMenu4 = new JMenu();
		jMenu5 = new JMenu();
		jMenu7 = new JMenu();

		// Menu Bar:
		jMenuBar1 = new JMenuBar();
		jMenuBar2 = new JMenuBar();

		jMenuItem1 = new JMenuItem();
		jMenuItem2 = new JMenuItem();
		jMenuItem3 = new JMenuItem();
		jMenuItem4 = new JMenuItem();
		jMenuItem6 = new JMenuItem();
		jMenuItem7 = new JMenuItem();
		jMenuItem8 = new JMenuItem();

		// Label:
		jLabel1 = new JLabel();
		jLabel2 = new JLabel();
		jLabel3 = new JLabel();
		txt_emp = new JLabel();
		// set Username to Label:
		txt_emp.setText(Emp.requestUsername());


		// Scroll Pane:
		jScrollPane1 = new JScrollPane();

		// List:

		// ----------------------------------------------------Theme-----------------------------------------------------------------//

		/*
		 * Set theme font, color and background
		 */
		Theme t = new Theme();
		int _fontsize = t.getSize();
		_font = new Font(Font.SERIF, 1, _fontsize);
		_icon = new ImageIcon(getClass().getResource(t.getThemeBackground()));
		_fontColor = t.getColor();

		// ----------------------------------------------------Theme-----------------------------------------------------------------//

		jMenu3.setText(messages.getString("file"));
		jMenuBar2.add(jMenu3);

		jMenu4.setText(messages.getString("edit"));
		jMenuBar2.add(jMenu4);

		// jframe exit listener
		WindowListener exitListener = new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				// TODO: set localization specific dialog
				int confirm = JOptionPane.showOptionDialog(null, "Are You Sure to Close Application?",
						"Exit Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
				System.out.println("");
				if (confirm == 0) {
					System.exit(0);
				}
			}
		};
		addWindowListener(exitListener);

		setResizable(false);

		jPanel2.setLayout(null);

		// ----------------------------------------------------Buttons-----------------------------------------------------------------//

		
		initializeAllowanceButtonUI();
		

		initializeRPGButtonUI();

		// all all uifeatures to arraylist
		addUIFeatureToList();
		// logout button is handled differently
		setLogoutButton();
		// add all features in arraylist to panel:
		putAllSelectedFeaturesToPanel();

		//set buttons to specified location on jpanel:
		setAllSelectedFeaturesToLocation();

		// --------------------------------------------------</Buttons>----------------------------------------------------------------//

		// ------------------------------------------------------Labels-----------------------------------------------------------------//
		// ------Label: Logged in as------//
		jLabel1.setForeground(new java.awt.Color(255, 255, 255));
		jLabel1.setText(messages.getString("logged_in_as") + " :");
		jPanel2.add(jLabel1);
		jLabel1.setBounds(10, window_height - 100, 180, 14);

		txt_emp.setForeground(new java.awt.Color(255, 255, 255));
		jPanel2.add(txt_emp);
		txt_emp.setBounds(130, window_height - 100, 80, 14);

		jLabel2.setIcon(_icon);
		jPanel2.add(jLabel2);
		jLabel2.setBounds(0, 0, window_width + 20, window_height); // TODO: adjust background image size
		pack();

		// Pack JFrame
		pack();
		// ----------------------------------------------------</Labels>----------------------------------------------------------------//

		// -------------------------------------------------------Menu------------------------------------------------------------------//

		jMenu7.setText(messages.getString("about"));
		jMenuItem3.setText("Version 1.0");
		jMenu7.add(jMenuItem3);
		jMenuItem4.setText("Support");
		jMenu7.add(jMenuItem4);
		jMenuItem8.setText("About EMS project");
		jMenu7.add(jMenuItem8);
		jMenuBar1.add(jMenu7);

		setJMenuBar(jMenuBar1);

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jPanel2,
				GroupLayout.DEFAULT_SIZE, 996, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jPanel2,
				GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE));

		// maximize Main Menu JFrame
//		this.setExtendedState(getExtendedState() | MAXIMIZED_BOTH); 
		setPreferredSize(new Dimension(window_width, window_height));
		pack();

	}// GEN-END:initComponents

	/**
	 * 
	 */
	private void setLogoutButton() {
		// Logout is handled differently:
		jButton6.setText(messages.getString("logout"));
		jButton6.setBackground(new java.awt.Color(255, 255, 255));
		jButton6.setFont(_font);
		jButton6.setForeground(_fontColor);
		jButton6.setIcon(new ImageIcon(getClass().getResource("/Images/logout.png"))); // NOI18N
		jButton6.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				buttonActionLogout(evt);
			}
		});

		int logoutButtonLength = 150;
		jPanel2.add(jButton6);
		jButton6.setBounds(window_width - logoutButtonLength - 10, 10, 150, 50);
	}

	// ------------------------------------------------------------------------------------------------------------------------------------------//

	// ------------------------------------------------------------------------------------------------------------------------------------------//

	// ---------------------------------------------------------ACTION_HANDLER-------------------------------------------------------------------//





	/**
	 * Button: Logout
	 * 
	 * @param evt
	 */
	private void buttonActionLogout(ActionEvent evt) {// GEN-FIRST:event_buttonActionLogout
		// close Main Menu
		this.dispose();

		// open login window
		login x = new login();
		x.setVisible(true);
		writeLogoutToAuditDetails();

	}// GEN-LAST:event_buttonActionLogout

	/**
	 * 
	 * @param evt
	 */
	private void itemActionGenerateEmpReport(ActionEvent evt) {// GEN-FIRST:event_itemActionGenerateEmpReport
		// Dialog:
		JFileChooser dialog = new JFileChooser();
		dialog.setSelectedFile(new File("Employees Report.pdf"));
		int dialogResult = dialog.showSaveDialog(null);

		if (dialogResult == JFileChooser.APPROVE_OPTION) {
			String filePath = dialog.getSelectedFile().getPath();

			printEmployeeReportAsPDF(filePath);

		}

	}// GEN-LAST:event_itemActionGenerateEmpReport

	/**
	 * 
	 * @param evt
	 */
	public void itemActionGenerateAllowanceReport(ActionEvent evt) {// GEN-FIRST:event_itemActionGenerateAllowanceReport

		// TODO put method into class Allowance
		JFileChooser dialog = new JFileChooser();
		dialog.setSelectedFile(new File("Employee Allowance Report.pdf"));
		int dialogResult = dialog.showSaveDialog(null);
		if (dialogResult == JFileChooser.APPROVE_OPTION) {
			String filePath = dialog.getSelectedFile().getPath();

			printAllowanceReportAsPDF(filePath);
			JOptionPane.showMessageDialog(null, messages.getString("report_sucessful"));

		}

	}// GEN-LAST:event_itemActionGenerateAllowanceReport



	/**
	 * Item:
	 * 
	 * @param evt
	 */
	private void itemActionGenerateEmpDeductionsReport(ActionEvent evt) {// GEN-FIRST:event_itemActionGenerateEmpDeductionsReport

		// #ifdef ApplicationLogging
		// @ log.info("Starting PDF export for employee deduction report");
		// #endif
		JFileChooser dialog = new JFileChooser();
		dialog.setSelectedFile(new File("Employee Deduction Report.pdf"));
		int dialogResult = dialog.showSaveDialog(null);
		if (dialogResult == JFileChooser.APPROVE_OPTION) {
			String filePath = dialog.getSelectedFile().getPath();

			printEmployeeDeductionReportAsPDF(filePath);
		}

	}// GEN-LAST:event_itemActionGenerateEmpDeductionsReport

	/**
	 * Base - Main Menu
	 * 
	 * @param args
	 *            no runtime parameter
	 */
	public static void main(String args[]) {

		// Display MainMenu Runnable:
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MainMenu().setVisible(true);
			}
		});
	}

	// #ifdef ApplicationLogging
	// @ Logger log = new
	// @ de.ovgu.isp.ems.logging.ApplicationLogger().getApplicationLogger();
	// #endif

	ResourceBundle messages = new Localization().getBundle();
	private Font _font; // theme font
	private Color _fontColor; // Text color
	private ImageIcon _icon; // background wallpaper
	// Store all Buttons in a separated array:
	private JButton[] buttonsArray;

	JButton jButton2 = new JButton();
	JButton jButton3 = new JButton();
	JButton jButton4 = new JButton();
	JButton jButton5 = new JButton();
	JButton jButton6 = new JButton();
	JButton jButton7 = new JButton();
	JButton jButton8 = new JButton();
	// Buttons:

	private JMenu jMenu1;
	private JMenu jMenu3;
	private JMenu jMenu4;
	private JMenu jMenu5;
	private JMenu jMenu7;
	// Menu Bar
	private javax.swing.JMenuBar jMenuBar1;
	private javax.swing.JMenuBar jMenuBar2;
	// Menu Items
	private javax.swing.JMenuItem jMenuItem1;
	private javax.swing.JMenuItem jMenuItem2;
	private javax.swing.JMenuItem jMenuItem3;
	private javax.swing.JMenuItem jMenuItem4;
	private javax.swing.JMenuItem jMenuItem6;
	private javax.swing.JMenuItem jMenuItem7;
	private javax.swing.JMenuItem jMenuItem8;
	// Panel:
	private javax.swing.JPanel jPanel2;
	// Scroll Pane:
	private javax.swing.JScrollPane jScrollPane1;
	// Text Field:
	// Label:
	private javax.swing.JLabel txt_emp;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;

	// List:

}
