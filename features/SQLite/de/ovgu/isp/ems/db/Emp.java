package de.ovgu.isp.ems.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Get Employee ID from this Class
 * 
 * @author fs
 *
 */
public class Emp {

	// TODO: refactor static access

	public static int empId;
	static Connection conn = db.java_db();
	static String userName = requestUsername();
	static ResultSet rs = null;
	static PreparedStatement pst = null;

	/**
	 * 
	 * @return
	 */
	public static String requestUsername() {

		String sql = "SELECT username FROM USERS WHERE id = " + empId;
		try {
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			String _user = "";
			while (rs.next()) {
				_user = rs.getString(1);
			}

			return _user;

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("returns NO_USER");
			return "NO_USER";

		}

	}

}
