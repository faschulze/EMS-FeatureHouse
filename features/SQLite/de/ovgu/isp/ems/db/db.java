package de.ovgu.isp.ems.db;

import java.io.File;
import java.sql.Connection;
import javax.swing.JOptionPane; //only required for testing db conn
import java.sql.DriverManager;

//#ifdef ApplicationLogging
//@ import de.ovgu.isp.ems.logging.ApplicationLogger;
//@ import org.apache.logging.log4j.Logger;
//@ import de.ovgu.isp.ems.db.Emp;
//#endif

//TODO: !! DB list all table names and attributes

/**
 * Setting up connection to SQLite Database
 * 
 * The database itself is located inside the empnet.sqlite file.
 * 
 * Following TABLES are included in the database:
 * 
 * TABLE USERS
 * 
 * TABLE AUDIT_DETAIL
 * 
 * TABLE STAFF_INFORMATION
 * 
 * TABLE
 * 
 * 
 * @author fs
 *
 */
public class db {

	static Connection conn = null;

	/**
	 * This static method is used to get the current database connection.
	 * 
	 * @return DB connection to SQLite Database
	 */
	public static Connection java_db() {
		try {
			Class.forName("org.sqlite.JDBC");
			Connection conn = DriverManager
					.getConnection("jdbc:sqlite:." + File.separator + "src" + File.separator + "empnet.sqlite");
			/*
			 * test the db connection:
			 * 
			 * JOptionPane.showMessageDialog(null, "Connection to database successfully"+
			 * "established.");
			 */

			// #ifdef ApplicationLogging
			// @ Logger log = new ApplicationLogger().getApplicationLogger();
			// @ log.info("DB call - database connection established by user " + Emp.empId);
			// #endif

			return conn;
		} catch (Exception e) {
			// display error message dialog:
			JOptionPane.showMessageDialog(null, e);
			e.printStackTrace();
			// #ifdef ApplicationLogging
			// @ Logger log = new ApplicationLogger().getApplicationLogger();
			// @ log.fatal("Failed to open database connection to SQLite.");
			// @ log.error(e.getStackTrace());
			// #endif
			return null;
		}

	}

	// empty constructor:
	public db() {

	}
}
