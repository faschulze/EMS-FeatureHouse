package de.ovgu.isp.ems.core;


import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Localization {

	public String getCountrySpecificDate() {
		Calendar cal = new GregorianCalendar();
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		return " | " + (month + 1) + "-" + day + "-" + year + " | ";
	}

	// get locale germany
	public Locale getCurrentLocale() {
		Locale _locale = new Locale("ru", "RU");
		return _locale;
	}
}
