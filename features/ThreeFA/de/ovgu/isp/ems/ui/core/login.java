import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 
 * @author fs
 *
 */
public class login {

	// JLabel:
	JLabel jLabel3 = new JLabel();
	// JPanel:
	JPanel jPanel2 = new JPanel();
	// JComboBox: Select List
	private JComboBox txt_combo = new JComboBox();

	/**
	 * This method initializes UI fields consisting of user name, password und user
	 * role fields
	 */
	public void initUIfields() {

		// get all listed user roles from feature AccessController:
		String[] roles = new AccessControl().getAllUserRoles();
		// set all user roles to JComboBox:
		txt_combo.setModel(new DefaultComboBoxModel(roles));
		jPanel2.add(txt_combo);
		txt_combo.setBounds(100, 430, 130, 30);
		jLabel3.setForeground(new java.awt.Color(255, 255, 255));
		jLabel3.setText(messages.getString("selectrole") + ":");
		jPanel2.add(jLabel3);
		jLabel3.setBounds(10, 440, 100, 14);

	}

	/**
	 * @return selected user role needed to perform login
	 */
	public String getSelectedUserRole() {
		return txt_combo.getSelectedItem().toString();
	}

	/**
	 * set selected user role to prepared sql statement to perform
	 * login with three factors
	 * 
	 */
	public void putRoleToSQL() {
		try {
			pst.setString(3, txt_combo.getSelectedItem().toString()); // only if 3FA
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
