package de.ovgu.isp.ems.core;

/**
 * 
 * @author fs
 *
 */
public class Theme {

	// <Feature: Theme3>
	String _imageLocation = "/Images/bk3.jpg";

	/**
	 * 
	 * @return the theme's background image location
	 */
	public String getThemeBackground() {
		return _imageLocation;
	}
	// </Feature:Theme3>

}